#! /usr/bin/env python

import asyncio
import websockets
import ssl
import pathlib
import time

# Logging
LOG_INFO = lambda s : print(time.strftime("[%d%m%y-%H%M%S]") + "[SERVER]\033[92m [INFO]\033[0m: " + str(s)) # Info
LOG_WARN = lambda s : print(time.strftime("[%d%m%y-%H%M%S]") + "[SERVER]\033[93m [WARN]\033[0m: " + str(s)) # Warning
LOG_FAIL = lambda s : print(time.strftime("[%d%m%y-%H%M%S]") + "[SERVER]\033[91m [FAIL]\033[0m: " + str(s)) # Failure


async def run(ws, path):
    while True:
        # TODO
        await ws.send("test123")
        time.sleep(1)


LOG_INFO("Initialising SSL")
ssl_context  = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
ssl_cert = pathlib.Path(__file__).with_name("root.crt")
ssl_key  = pathlib.Path(__file__).with_name("root.key")
ssl_context.load_cert_chain(ssl_cert, keyfile=ssl_key)


LOG_INFO("Starting")
asyncio.get_event_loop().run_until_complete(websockets.serve(run, "localhost", 9999, ssl=ssl_context))
asyncio.get_event_loop().run_forever()
