#! /usr/bin/env python

import asyncio
import websockets
import pathlib
import ssl
import time


# Logging
LOG_INFO = lambda s : print(time.strftime("[%d%m%y-%H%M%S]") + "[CLIENT]\033[92m [INFO]\033[0m: " + str(s)) # Info
LOG_WARN = lambda s : print(time.strftime("[%d%m%y-%H%M%S]") + "[CLIENT]\033[93m [WARN]\033[0m: " + str(s)) # Warning
LOG_FAIL = lambda s : print(time.strftime("[%d%m%y-%H%M%S]") + "[CLIENT]\033[91m [FAIL]\033[0m: " + str(s)) # Failure


async def onMessage(uri):
    async with websockets.connect(uri) as ws:
        while True:
            # TODO
            x = await ws.recv()
            print(x)
            time.sleep(1)


asyncio.get_event_loop().run_until_complete(onMessage("wss://localhost:9999"))
